namespace PartsManagement.SharedDomain.Models.Conversion
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Extensions;
    using DocumentBase;
    using ConvertedValues;
    using PartsManagementIO.DomainService.Interfaces.Documents;
    using Interfaces;
    using PartBase;
    using Property;

    public abstract class ConversionSchemaItem
    {
        public string Convert { get; set; }

        public List<Tuple<string, string>> Params { get; set; } = new List<Tuple<string, string>>();

        protected IConversionSchemaItemImpl ConverterImplementation { get; set; }

        public bool InitConverter(IGetModel<Document> documentGetModel, ILanguageMap languageMap, string cultureInfo, IList<Tuple<Document, IManagedData>> createdDocuments)
        {
            if (this.ConverterImplementation != null)
            {
                return true;
            }

            return this.InitConverter(this.Convert, documentGetModel, languageMap, cultureInfo, createdDocuments) != null;
        }

        public abstract Task<ITargetSystemValue[]> ProcessFromCpms(
            Part part,
            IGetModel<Document> documentGetModel,
            ILanguageMap languageMap,
            string cultureInfo,
            bool isVariantProperty);

        public abstract Task<bool> ProcessToCpms(
            IGetModel<Document> documentGetModel,
            Func<string, bool, string> funcValueGetter,
            Part targetPart,
            ILanguageMap languageMap,
            string cultureInfo,
            IList<Tuple<Document, IManagedData>> createdDocuments);

        public abstract ITargetSystemValue ConvertEmptyFromCpms(bool isVariantProperty);

        protected async Task<List<ITargetSystemValue>> ConvertSchemaItem(Part part, string propertyName, bool isVariantProperty)
        {
            List<ITargetSystemValue> result = await this.ConvertProperties(part, propertyName, isVariantProperty);

            // the propertyName wasn't found in properties, try it in structures
            if (result == null || result.IsEmpty())
            {
                result = await this.ConvertStructures(part, propertyName, isVariantProperty);
            }

            return result ?? new List<ITargetSystemValue>();
        }

        protected abstract IConversionSchemaItemImpl InitConverter(string value, IGetModel<Document> documentGetModel, ILanguageMap languageMap, string cultureInfo, IList<Tuple<Document, IManagedData>> createdDocuments);

        private async Task<List<ITargetSystemValue>> ConvertStructures(Part part, string propertyName, bool isVariantProperty)
        {
            List<Tuple<StructureValue, StructureValue>> structures = part.GetStructuresInDeep(propertyName);
            return await this.ConvertValues(structures, propertyName, part, isVariantProperty);
        }

        private async Task<List<ITargetSystemValue>> ConvertProperties(Part part, string propertyName, bool isVariantProperty)
        {
            List<Tuple<StructureValue, PropertyValue>> properties = part?.GetPropertiesInDeep(propertyName);

            return await this.ConvertValues(properties, propertyName, part, isVariantProperty);
        }

        private async Task<List<ITargetSystemValue>> ConvertValues<T>(List<Tuple<StructureValue, T>> values, string propertyName, Part part, bool isVariantProperty)
            where T : IIdentifiableElement
        {
            if (values == null || values.IsEmpty())
            {
                return null;
            }

            List<ITargetSystemValue> results = new List<ITargetSystemValue>();
            foreach (Tuple<StructureValue, T> value in values)
            {
                ITargetSystemValue result = await this.ConverterImplementation.ConvertFromCpms(
                    value.Item2,
                    propertyName,
                    part,
                    isVariantProperty);
                if (result != null)
                {
                    result.ParentStructure = value.Item1;
                    results.Add(result);
                }
            }

            return results;
        }
    }
}
