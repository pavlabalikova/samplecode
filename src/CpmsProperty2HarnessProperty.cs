namespace PartsManagementIO.Domain.Models.Conversion.Impl
{
    using System.Threading.Tasks;
    using ConvertedValues;
    using Property;
    using Types;

    internal class CpmsProperty2HarnessProperty : ConversionSchemaItemImpl<ConversionSchemaSimpleItem, PropertyValue>
    {
        public CpmsProperty2HarnessProperty(ConversionSchemaSimpleItem conversionSchemaItem)
           : base(conversionSchemaItem)
        {
        }

        protected override Task<ITargetSystemValue> ConvertFromCpms(
            PropertyType propertyType,
            PropertyValue propertyValue)
        {
            if (propertyValue == null)
            {
                return null;
            }

            ITargetSystemValue hpdProperty = HarnessValueConverter
                .GetProperty(propertyValue, propertyType, this.ConversionSchemaItem.Out);

            return Task.FromResult(hpdProperty);
        }

        protected override Task<PropertyValue> ConvertValueToCpms(string targetPropertyIdentifier, string foreignValue)
        {
            return Task.FromResult(new PropertyValue { Identifier = targetPropertyIdentifier, Value = foreignValue });
        }
    }
}
