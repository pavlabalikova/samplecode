namespace PartsManagement.SharedDomain.Models.Property
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Interfaces;

    /// <summary>
    /// Holds structured relations between nodes/objects
    /// </summary>
    /// <typeparam name="T">Identifier of the node</typeparam>
    /// <typeparam name="U">Relation between nodes</typeparam>
    /// <typeparam name="V">The node itself</typeparam>
    public class RelationHolder<T, U, V>
        where T : class
        where U : IRelation<T>
        where V : class
    {

        private Dictionary<T, ObjectInRelation> relatedNodes;
        private Dictionary<Tuple<T, T>, U> reationsDictionary = new Dictionary<Tuple<T, T>, U>();
        private Dictionary<T, List<V>> listOfSlavesByMasterKey = new Dictionary<T, List<V>>();

        public RelationHolder(Dictionary<T, V> relatedStructures)
        {
            this.relatedNodes = relatedStructures.ToDictionary(dic => dic.Key, dic => new ObjectInRelation(dic.Value));
        }

        public bool AddRelation(U relation)
        {
            Tuple<T, T> key = Tuple.Create(relation.From, relation.To);
            V fromObject = this.GetRelatedObject(relation.From);
            V toObject = this.GetRelatedObject(relation.To);
            if (this.reationsDictionary.ContainsKey(key) || fromObject == null || toObject == null)
            {
                return false;
            }

            this.reationsDictionary.Add(key, relation);
            this.AddSlave(relation.From, toObject);
            return true;
        }

        public bool IsSlave(T identifier)
        {
            this.relatedNodes.TryGetValue(identifier, out ObjectInRelation objectInRelation);
            return objectInRelation?.IsSlave ?? false;
        }

        public IEnumerable<V> GetSlaves(T identifier)
        {
            this.listOfSlavesByMasterKey.TryGetValue(identifier, out List<V> slaves);
            return slaves ?? new List<V>();
        }

        private void AddSlave(T from, V slave)
        {
            if (slave == null)
            {
                return;
            }

            if (!this.listOfSlavesByMasterKey.ContainsKey(from))
            {
                this.listOfSlavesByMasterKey.Add(from, new List<V>());
            }

            this.listOfSlavesByMasterKey[from].Add(slave);
            this.relatedNodes[from].IsSlave = true;
        }

        private V GetRelatedObject(T identifier)
        {
            if (identifier != null && this.relatedNodes.ContainsKey(identifier))
            {
                return this.relatedNodes[identifier].OriginalObject;
            }

            return null;
        }

        private class ObjectInRelation
        {
            public ObjectInRelation(V originalObj)
            {
                this.OriginalObject = originalObj;
            }

            public V OriginalObject { get; }

            public bool IsSlave { get; set; }
        }
    }
}
