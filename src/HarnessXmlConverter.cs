namespace PartsManagementIO.AppService.Converters.Harness
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Linq;
    using PartsManagement.SharedDomain.Common.Application;
    using PartsManagement.SharedDomain.Common.Extensions;
    using PartsManagement.SharedDomain.Models.Constants;
    using PartsManagement.SharedDomain.Models.Conversion.ConvertedValues;
    using PartsManagement.SharedDomain.Models.Interfaces;
    using PartsManagement.SharedDomain.Models.PartBase;
    using PartsManagement.SharedDomain.Models.Property;
    using DomainService.Interfaces.ConversionSchema;
    using DomainService.Interfaces.Documents;

    using Microsoft.WindowsAzure.Storage;
    using Microsoft.WindowsAzure.Storage.Blob;

    /// <summary>
    /// Converts data model to Harness pro D import format.
    /// </summary>
    public class HarnessXmlConverter : XmlConverter
    {
        private readonly IDocumentService documentService;
        private readonly CloudBlobContainer blobContainer;
        private readonly IConversionSchemaService conversionSchemaService;

        public HarnessXmlConverter(
            IConversionSchemaService conversionSchemaService,
            IDocumentService documentService,
            IPartsManagementIdentity identity,
            IStorageSettings settings)
        {
            this.conversionSchemaService = conversionSchemaService;
            this.documentService = documentService;
            if (settings != null)
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(settings.DocumentStoreToken);
                this.blobContainer = storageAccount.CreateCloudBlobClient().GetContainerReference(identity.Organization.ToString());
            }
        }

        /// <inheritdoc />
        protected override async Task<bool> ConvertPartToXml(XmlWriter xml, Part part)
        {
            HarnessPart harnessPart = new HarnessPart(part);
			      // Converts values from cpms to harness suitable format by using conversion schema
            ITargetSystemValue[] targetSystemValues = await this.conversionSchemaService.ConvertDataFromCpms(ConversionSchemas.HarnessConversionSchema, part);
            HarnessPropertyResultXElementVisitor harnessPropertyVisitor = new HarnessPropertyResultXElementVisitor();
            DocumentConverter documentConverter = new DocumentConverter(this.documentService, this.blobContainer);
            HarnessDocumentsResultXElementVisitor harnessDocumentVisitor = new HarnessDocumentsResultXElementVisitor(documentConverter);

            foreach (ITargetSystemValue key2Value in targetSystemValues.WhereNotNull())
            {
                await key2Value.AcceptVisitor(harnessPropertyVisitor);
                await key2Value.AcceptVisitor(harnessDocumentVisitor);
            }

            XElement partElement = new XElement("part");
            this.ConvertPart(partElement, harnessPart);
            this.ConvertPropertiesToXml(
                partElement,
                harnessPart.Values,
                harnessPropertyVisitor.HarnessPropertiesAsXElements,
                harnessPropertyVisitor.HarnessStructurePropertiesAsXElements);
            this.ConvertDocuments(partElement, harnessDocumentVisitor.HarnessDocumentsAsXElements);

            // Write final document to xml
            partElement.WriteTo(xml);
            return true;
        }

        private void ConvertPart(XElement xmlElement, HarnessPart harnessPart)
        {
            xmlElement.Add(new XAttribute("guid", harnessPart.Id.ToString()));
            xmlElement.Add(new XAttribute("categoryType", harnessPart.HarnessCategoryType));
        }

        private void ConvertDocuments(XElement parentElement, List<XElement> harnessDocuments)
        {
            XElement documentsElement = new XElement("documents");
            harnessDocuments.WhereNotNull().ForEach(element => documentsElement.Add(element));
            parentElement.Add(documentsElement);
        }

        private void ConvertPropertiesToXml(
            XElement xmlElement,
            ISinglePropertyValues propertyValues,
            Dictionary<Tuple<StructureValue, string>, XElement> convertedProperties,
            Dictionary<StructureValue, XElement> convertedStructuredProperties)
        {
            XElement properties = new XElement("properties");
            propertyValues?.Properties?.ForEach(prop =>
                {
                    Tuple<StructureValue, string> key = Tuple.Create(propertyValues as StructureValue, prop.Identifier);
                    if (prop.Identifier == null || !convertedProperties.ContainsKey(key))
                    {
                        return;
                    }

                    properties.Add(convertedProperties[key]);
                });
            xmlElement.Add(properties);
            XElement structuralProperties = new XElement("structuralProperties");
            propertyValues?.Structures?.ForEach(structure =>
            {
                if (structure.Identifier == null || !convertedStructuredProperties.ContainsKey(structure))
                {
                    return;
                }

                XElement structuralProperty = convertedStructuredProperties[structure];
                this.ConvertPropertiesToXml(structuralProperty, structure, convertedProperties, convertedStructuredProperties);
                structuralProperties.Add(structuralProperty);
            });
            xmlElement.Add(structuralProperties);
        }
    }
}
